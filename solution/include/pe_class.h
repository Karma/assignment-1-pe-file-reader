/// @file pe_class.h
/// @brief Mодуль определяющий структуры PE файла

#pragma once

#include <pe_class.h>
#include <stdint.h>
#include <stdio.h>

/// Сдвиг заголовка
#define MAIN_DELTA 0x3c

/// Определящее PE файл
#define PE_MAGIC_NUMBER 0x00004550

/**
    @struct PEHeader
    @brief заголовок PE файла
*/

#ifdef _MSC_VER
#pragma pack(push, 1)
#endif
struct 
#ifdef __GNUC__
        __attribute__((packed))
#endif
 PEHeader{
    uint32_t magic;
    uint16_t machine;
    uint16_t numberOfSections;
    uint32_t timeDateStamp;
    uint32_t pointerToSymbolTable;
    uint32_t numberOfSymbols;
    uint16_t sizeOfOptionalHeader;
    uint16_t characteristics;
};

/**
    @struct SectionHeader
    @brief заголовок секции
*/
#ifdef _MSC_VER
#pragma pack(push, 1)
#endif
struct 
#ifdef __GNUC__
        __attribute__((packed))
#endif
SectionHeader {
    char name[8];
    uint32_t virtualSize;
    uint32_t virtualAddres;
    uint32_t sizeOfRawData;
    uint32_t pointerToRawData;
    uint32_t pointerToRelocations;
    uint32_t pointerToLinenumbers;
    uint16_t numberOfRelocations;
    uint16_t numberOfLinenumbers;
    uint32_t characteristics;
};


/**
    @struct PEFile
    @brief PE файл со смещениями
*/
#ifdef _MSC_VER
#pragma pack(push, 1)
#endif
struct 
#ifdef __GNUC__
        __attribute__((packed))
#endif
 PEFile {
    uint32_t magic_offset;
    uint32_t header_offset;
 
    uint32_t magic;
    struct PEHeader header;
    struct SectionHeader *section_headers;
};
